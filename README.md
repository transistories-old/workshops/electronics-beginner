# Workshop Electronics

An introductory workshop around electronics

The workshop is organized as a set of small (documented) projects that the students can complete on their own or ask for assitance if needed.

Links to extra (theoretical) information are provided, for those wanting to known more about why and how certain things work. But this information is considered as not needed for the workshop.

All that is required for the workshop is an interest in electronics and a movitvation to learn!

**Due to the ongoing Corona crisis the types of projects is limited; since shared equipement, such as oscilloscopes, multimeters etc is out of the question...**

Docs can be found at [http://www.transistories.org/workshops/electronics-beginner/](http://www.transistories.org/workshops/electronics-beginner/ "Workshop documentation")

A complete Bill of Materials (with prices) can be fount at: [http://www.transistories.org/workshops/electronics-beginner/#bill_of_materials](http://www.transistories.org/workshops/electronics-beginner/#bill_of_materials "Workshop materials")

## Projects

### 7805 Breadboard Power Supply

A simple linear power supply to power the rest of the projects. I uses a 9v battery as a energy source.

A PTC resistors is used as a resettable fuse.

![7805 schematic](docs/diagrams/7805.jpg "7805 schematic")

## Projects to work out

- simple circuits
    - LED resistor
        - basic concepts of resistors and current limiting
        - ohm's- & khirchoff's laws
    - switch debounce
        - pull-up/down resistors
        - switch bounce and it's side-effects
        - schmitt triggers
        - power-on reset & manual reset circuitry
- 555 timer IC
    - Bistable mode (SR flip-flop)
    - Bistable mode (T flip-flop)?
    - Monostable mode (pulse extension)
    - Astable mode (oscillator / clock)
- Meet the BC547 & BC557?
- TTL logic gates
    - Inverter
        - NPN based inverter
            ![NPN inverter](docs/diagrams/NPN_inverter.jpg "NPN inverter")
            
            | A | Y | mode    |
            | - | - | ------- |
            | 0 | 1 | pull-up |
            | 1 | 0 | sink    |
        
        - PNP based inverter
            ![PNP inverter](docs/diagrams/PNP_inverter.jpg "PNP inverter")
            
            | A | Y | mode      |
            | - | - | --------- |
            | 0 | 1 | source    |
            | 1 | 0 | pull-down |
    
    - NAND gate
        ![NAND gate](docs/diagrams/NAND_gate.jpg "NAND gate")

        | A | B | Y | mode |
        | - | - | - | ---- |
        | 0 | 0 | 1 | pull-up |
        | 0 | 1 | 1 | pull-up |
        | 1 | 0 | 1 | pull-up |
        | 1 | 1 | 0 | sink |

    - OR gate

        | A | B | Y | mode |
        | - | - | - | ---- |
        | 0 | 0 | 0 |  |
        | 0 | 1 | 1 |  |
        | 1 | 0 | 1 |  |
        | 1 | 1 | 1 |  |

    - AND gate
        ![AND gate](docs/diagrams/AND_gate.jpg "AND gate")

        | A | B | Y | mode |
        | - | - | - | ---- |
        | 0 | 0 | 0 | pull-down |
        | 0 | 1 | 0 | pull-down |
        | 1 | 0 | 0 | pull-down |
        | 1 | 1 | 1 | source |
    
    - NAND as universal gate
    - DTL
    - Output types
        - open-collector
        - open-emitter
        - push-pull
        - tri-state
    - Input types
        - schmitt trigger
- 74xx series logic
    - 7400 Quad NAND
    - 7402 Quad NOR?
    - 7404 Hex inverter?
    - 7408 Quad AND?
    - 7414 Hex inverter (Schmitt trigger)
    - 7432 Quad OR?
    - 7486 Quad EXOR?