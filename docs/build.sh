#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

python "src/bom_gen.py" "$DIR"

media_folder="$1/assets/media/workshops/electronics-beginner/"
mkdir -p "$media_folder"

# Copy over the diagrams folder to the assets folder
cp -r "$DIR/diagrams/." "$media_folder/diagrams/"

# Copy over the schematics folder to the downloads folder
# TODO